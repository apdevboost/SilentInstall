package com.example.codemate.silentinstall.model

import android.os.Parcel
import android.os.Parcelable

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class ForceUpdateDao(var isMaintain: Boolean, var descMaintain: String, var isForceUpdate: Boolean, var androidVersionCode: Int, var appLink: String): Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readByte() != 0.toByte(),
            parcel.readString(),
            parcel.readByte() != 0.toByte(),
            parcel.readInt(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeByte(if (isMaintain) 1 else 0)
        parcel.writeString(descMaintain)
        parcel.writeByte(if (isForceUpdate) 1 else 0)
        parcel.writeInt(androidVersionCode)
        parcel.writeString(appLink)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ForceUpdateDao> {
        override fun createFromParcel(parcel: Parcel): ForceUpdateDao {
            return ForceUpdateDao(parcel)
        }

        override fun newArray(size: Int): Array<ForceUpdateDao?> {
            return arrayOfNulls(size)
        }
    }


}
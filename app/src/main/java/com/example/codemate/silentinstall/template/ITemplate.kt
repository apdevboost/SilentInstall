package com.example.codemate.silentinstall.template

import com.example.codemate.silentinstall.base.BasePresenter

interface ITemplate {

    interface View

    interface Presenter: BasePresenter

}

package com.example.codemate.silentinstall.service
import android.app.IntentService
import android.content.Intent
import android.os.Environment
import android.support.v4.content.LocalBroadcastManager
import android.util.Log
import com.example.codemate.silentinstall.model.DownloadDao

import com.liulishuo.filedownloader.BaseDownloadTask
import com.liulishuo.filedownloader.FileDownloadListener
import com.liulishuo.filedownloader.FileDownloader

import java.io.File

import okhttp3.ResponseBody
import retrofit2.Call

class DownloadService : IntentService("Download Service") {

    private var mId: Int = 0
    private var mUrl: String? = null
    private var mName: String? = null
    private var mPath: String? = null
    private var request: Call<ResponseBody>? = null

    private var totalFileSize: Int = 0

    override fun onHandleIntent(intent: Intent?) {
        mId = intent!!.extras!!.getInt(ID)
        mUrl = intent.extras!!.getString(URL)
        mName = intent.extras!!.getString(NAME)

        initFileDownloader()
    }

    private fun initFileDownloader() {
        Log.d("downloading", "mId: " + mId!!)
        Log.d("downloading", "url: " + mUrl!!)
        Log.d("downloading", "path: " + Environment.getExternalStorageDirectory().absolutePath + "/apps/data" + File.separator + mName + ".apk")
        FileDownloader.getImpl().create(mUrl)
                .setTag(mId)
                .setPath(Environment.getExternalStorageDirectory().absolutePath + "/apps/data" + File.separator + mName + ".apk")
                .setListener(object : FileDownloadListener() {
                    override fun pending(task: BaseDownloadTask, soFarBytes: Int, totalBytes: Int) {
                        val downloadDao = DownloadDao()
                        downloadDao.id = Integer.parseInt(task.tag.toString())
                        downloadDao.url = mUrl
                        downloadDao.fileName = mName
                        downloadDao.filePath = task.path
                        downloadDao.status = "pending"
                        sendDownloadBroadcast(downloadDao)
                        Log.d("downloading", "pending")
                    }

                    override fun started(task: BaseDownloadTask?) {
                        Log.i("downloading", "started")
                        val downloadDao = DownloadDao()
                        downloadDao.id = Integer.parseInt(task!!.tag.toString())
                        downloadDao.url = mUrl
                        downloadDao.fileName = mName
                        downloadDao.filePath = task.path
                        downloadDao.status = "started"
                        sendDownloadBroadcast(downloadDao)
                    }

                    override fun connected(task: BaseDownloadTask?, etag: String?, isContinue: Boolean, soFarBytes: Int, totalBytes: Int) {
                        Log.i("downloading", "connected")
                        val downloadDao = DownloadDao()
                        downloadDao.id = Integer.parseInt(task!!.tag.toString())
                        downloadDao.url = mUrl
                        downloadDao.fileName = mName
                        downloadDao.filePath = task.path
                        downloadDao.status = "connected"
                        sendDownloadBroadcast(downloadDao)
                    }

                    override fun progress(task: BaseDownloadTask, soFarBytes: Int, totalBytes: Int) {
                        Log.d("downloading", "$soFarBytes/$totalBytes")
                        val downloadDao = DownloadDao()
                        downloadDao.id = Integer.parseInt(task.tag.toString())
                        downloadDao.url = mUrl
                        downloadDao.fileName = mName
                        downloadDao.filePath = task.path
                        downloadDao.status = "progress"
                        downloadDao.progress = soFarBytes/totalBytes
                        downloadDao.soFarBytes = soFarBytes
                        downloadDao.totalBytes = totalBytes
                        sendDownloadBroadcast(downloadDao)
                    }

                    override fun blockComplete(task: BaseDownloadTask?) {
                        Log.d("downloading", "blockComplete")
                    }

                    override fun retry(task: BaseDownloadTask?, ex: Throwable?, retryingTimes: Int, soFarBytes: Int) {
                        Log.e("downloading", "retry# " + retryingTimes + " cause by " + ex!!.message)
                        val downloadDao = DownloadDao()
                        downloadDao.id = Integer.parseInt(task!!.tag.toString())
                        downloadDao.url = mUrl
                        downloadDao.fileName = mName
                        downloadDao.filePath = task.path
                        downloadDao.status = "retry"
                        downloadDao.isFailure = true
                        downloadDao.failureDescription = ex.message
                        sendDownloadBroadcast(downloadDao)
                    }

                    override fun completed(task: BaseDownloadTask) {
                        Log.d("downloading", "completed")
                        val downloadDao = DownloadDao()
                        downloadDao.id = Integer.parseInt(task.tag.toString())
                        downloadDao.url = mUrl
                        downloadDao.fileName = mName
                        downloadDao.filePath = task.path
                        downloadDao.status = "completed"
                        downloadDao.progress = 100
                        sendDownloadBroadcast(downloadDao)
                    }

                    override fun paused(task: BaseDownloadTask, soFarBytes: Int, totalBytes: Int) {
                        Log.w("downloading", "pause")
                        val downloadDao = DownloadDao()
                        downloadDao.id = Integer.parseInt(task.tag.toString())
                        downloadDao.url = mUrl
                        downloadDao.fileName = mName
                        downloadDao.filePath = task.path
                        downloadDao.status = "pause"
                        downloadDao.isFailure = true
                        downloadDao.progress = soFarBytes/totalBytes
                        downloadDao.failureDescription = "Cancel installation."
                        sendDownloadBroadcast(downloadDao)
                    }

                    override fun error(task: BaseDownloadTask, e: Throwable) {
                        Log.e("downloading", "error" + e.message)
                        val downloadDao = DownloadDao()
                        downloadDao.id = Integer.parseInt(task.tag.toString())
                        downloadDao.url = mUrl
                        downloadDao.fileName = mName
                        downloadDao.filePath = task.path
                        downloadDao.status = "error"
                        downloadDao.isFailure = true
                        downloadDao.failureDescription = e.message
                        sendDownloadBroadcast(downloadDao)
                    }

                    override fun warn(task: BaseDownloadTask) {
                        Log.w("downloading", "warn")
                        val downloadDao = DownloadDao()
                        downloadDao.id = Integer.parseInt(task.tag.toString())
                        downloadDao.url = mUrl
                        downloadDao.fileName = mName
                        downloadDao.filePath = task.path
                        downloadDao.status = "warning"
                        sendDownloadBroadcast(downloadDao)
                    }
                }).start()
    }

    private fun sendDownloadBroadcast(downloadDao: DownloadDao) {
        val intent = Intent(DOWNLOAD_STATUS)
        Log.d("download", "")
        intent.putExtra(DOWNLOAD, downloadDao)
        LocalBroadcastManager.getInstance(this@DownloadService).sendBroadcast(intent)
    }

    override fun onDestroy() {
        if (request != null) {
            request!!.cancel()
        }
        super.onDestroy()
    }

    companion object {

        val MESSAGE_PROGRESS = "message_progress"
        val DOWNLOAD_STATUS = "file_downloader_status"
        val DOWNLOAD = "download"
        val ID = "id"
        val URL = "url"
        val NAME = "name"
    }
}
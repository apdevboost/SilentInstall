package com.example.codemate.silentinstall.manager.http

import com.example.codemate.silentinstall.model.ForceUpdateDao
import io.reactivex.Flowable
import okhttp3.ResponseBody
import retrofit2.http.GET
import retrofit2.http.Path

interface Api {

    @GET("http://www.mocky.io/v2/5b34b0bb2f00008400376013")
    fun requestForceUpdate(): Flowable<ForceUpdateDao>
}
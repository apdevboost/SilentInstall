package com.example.codemate.silentinstall.model

import android.os.Parcel
import android.os.Parcelable

class DownloadDao : Parcelable {

    var id: Int = 0
    var progress: Int = 0
    var currentFileSize: Int = 0
    var totalFileSize: Int = 0
    var filePath: String? = null
    var fileName: String? = null
    var url: String? = null
    var isFailure: Boolean = false
    var failureDescription: String? = null
    var status: String? = null
    var soFarBytes: Int = 0
    var totalBytes: Int = 0

    constructor() {}

    protected constructor(`in`: Parcel) {
        id = `in`.readInt()
        progress = `in`.readInt()
        currentFileSize = `in`.readInt()
        totalFileSize = `in`.readInt()
        filePath = `in`.readString()
        fileName = `in`.readString()
        url = `in`.readString()
        isFailure = `in`.readByte().toInt() != 0
        failureDescription = `in`.readString()
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeInt(id)
        dest.writeInt(progress)
        dest.writeInt(currentFileSize)
        dest.writeInt(totalFileSize)
        dest.writeString(filePath)
        dest.writeString(fileName)
        dest.writeString(url)
        dest.writeByte((if (isFailure) 1 else 0).toByte())
        dest.writeString(failureDescription)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object {

        val CREATOR: Parcelable.Creator<DownloadDao> = object : Parcelable.Creator<DownloadDao> {
            override fun createFromParcel(`in`: Parcel): DownloadDao {
                return DownloadDao(`in`)
            }

            override fun newArray(size: Int): Array<DownloadDao?> {
                return arrayOfNulls(size)
            }
        }
    }
}
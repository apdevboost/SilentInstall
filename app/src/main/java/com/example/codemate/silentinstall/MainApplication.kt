package com.example.codemate.silentinstall

import android.app.Application
import com.example.codemate.silentinstall.manager.Contextor
import com.liulishuo.filedownloader.FileDownloader


class MainApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        // AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        Contextor.getInstance().init(applicationContext)
        FileDownloader.setup(applicationContext)
    }

}

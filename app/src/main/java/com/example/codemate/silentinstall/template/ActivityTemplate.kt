package com.example.codemate.silentinstall.template

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.example.codemate.silentinstall.R

class ActivityTemplate : AppCompatActivity(), ITemplate.View {

    private var presenter: PresenterTemplate? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.view_template)

        presenter = PresenterTemplate(this)

        initInstances()

        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                    .add(R.id.contentContainer, FragmentTemplate.newInstance())
                    .commit()
        }

    }

    private fun initInstances() {

    }

}

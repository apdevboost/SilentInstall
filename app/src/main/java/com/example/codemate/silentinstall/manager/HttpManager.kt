package com.example.codemate.silentinstall.manager

import com.example.codemate.silentinstall.manager.http.Api
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor

class HttpManager private constructor() {

    val service: Api

    init {
        val httpClient = OkHttpClient.Builder()

        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
//        httpClient.addInterceptor(logging)

        val client = httpClient.build()

        val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://www.mocky.io/v2/")
                .client(client)
                .build()

        service = retrofit.create(Api::class.java)
    }

    companion object {
        private var INSTANCE: HttpManager? = null
        fun getInstance(): HttpManager {
            if (INSTANCE == null) {
                INSTANCE = HttpManager()
            }
            return INSTANCE!!
        }
    }
}
package com.example.codemate.silentinstall.activity

import android.Manifest
import android.annotation.TargetApi
import android.app.Activity
import android.app.ProgressDialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.os.Parcelable
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.Toast
import com.example.codemate.silentinstall.R
import com.example.codemate.silentinstall.service.DownloadService
import com.example.codemate.silentinstall.service.DownloadService.Companion.DOWNLOAD
import com.example.codemate.silentinstall.service.DownloadService.Companion.DOWNLOAD_STATUS
import com.example.codemate.silentinstall.service.DownloadService.Companion.NAME
import com.example.codemate.silentinstall.service.DownloadService.Companion.URL
import com.example.codemate.silentinstall.service.InstallService
import com.example.codemate.silentinstall.service.InstallService.Companion.INSTALL
import com.example.codemate.silentinstall.service.InstallService.Companion.INSTALLING_PROGRESS
import com.example.codemate.silentinstall.service.InstallService.Companion.PATH
import com.example.codemate.silentinstall.manager.Contextor
import com.example.codemate.silentinstall.model.DownloadDao
import com.example.codemate.silentinstall.model.ForceUpdateDao
import com.example.codemate.silentinstall.model.InstallDao
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import kotlinx.android.synthetic.main.activity_main.*
import java.io.File

class MainActivity : AppCompatActivity(), IMainActivity.View {

    private val TAG = "MainActivity"
    private var presenter: MainActivityPresenterImpl? = null

    private var dao: ForceUpdateDao? = null
    private var bManager: LocalBroadcastManager? = null

    private lateinit var progressDialog: ProgressDialog
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        presenter = MainActivityPresenterImpl(this)

        initInstances()

    }

    private fun initInstances() {
        // Version Code Text
        activity_main_android_vc_tv.text = "Android Version Code: "+ packageManager.getPackageInfo(packageName, 0).versionCode

        // Progress Dialog
        progressDialog = ProgressDialog(this)
        progressDialog.setMessage("Loading ....")

        //Check if the application has required permissions
        checkPermissionGranted()

        // Update Button
        activity_main_install_btn.setOnClickListener {
            presenter?.onInstallClicked()
        }
    }

    private fun checkPermissionGranted() {
        Dexter.withActivity(this)
                .withPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(object : PermissionListener {
                    override fun onPermissionGranted(permissionGrantedResponse: PermissionGrantedResponse) {
                        Log.d(TAG, "checkForUpdates onPermissionGranted")
//                        presenter?.checkForUpdates()
                    }

                    override fun onPermissionDenied(permissionDeniedResponse: PermissionDeniedResponse) {
                        onBackPressed()
                    }

                    override fun onPermissionRationaleShouldBeShown(permissionRequest: PermissionRequest, permissionToken: PermissionToken) {
                        showPermissionRationale(permissionToken)
                    }
                })
                .withErrorListener { error -> Log.e(TAG, "There was an error: " + error.toString()) }
                .onSameThread()
                .check()
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun showPermissionRationale(token: PermissionToken) {
        AlertDialog.Builder(this).setTitle(R.string.permission_rationale_title)
                .setMessage(R.string.permission_rationale_message)
                .setNegativeButton(android.R.string.cancel, { dialog, which ->
                    dialog.dismiss()
                    token.cancelPermissionRequest()
                })
                .setPositiveButton(android.R.string.ok, { dialog, which ->
                    dialog.dismiss()
                    token.continuePermissionRequest()
                })
                .setOnDismissListener({ dialog -> token.cancelPermissionRequest() })
                .show()
    }

    /**
     * =================== Activity Lifecycle ===================
     */
    override fun onDestroy() {
        presenter?.viewOnDestroy()
        super.onDestroy()
    }

    /**
     * =================== Instance Save/Restore State ===================
     */

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        // Save Instance (Fragment level's variables) State here
        outState!!.putParcelable("ForceUpdateDao", dao)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        // Restore Instance (Fragment level's variables) State here
        dao = savedInstanceState.getParcelable<Parcelable>("ForceUpdateDao") as ForceUpdateDao?
    }

    /**
     * =================== BroadcastReceiver ===================
     */


    private val downloadBroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            if (intent.action == DOWNLOAD_STATUS) {
                val download = intent.getParcelableExtra<Parcelable>(DOWNLOAD)
                presenter?.onDownloadReceiver(download as DownloadDao)
            }
        }
    }

    private val applicationInstallBroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            if (intent.action == INSTALLING_PROGRESS) {
                val install = intent.getParcelableExtra<Parcelable>(INSTALL)
                presenter?.onApplicationInstallReceiver(install as InstallDao)
            }
        }
    }

    /**
     * ==================== Interface Callbacks ====================
     */

    override fun getActivity(): Activity {
        return this
    }


    override fun hideProgressBar() {
        progressDialog.hide()
    }

    override fun showProgressBar() {
        progressDialog.show()
    }

//    override fun showForceUpdateLayout() {
//        activity_main_force_update_fl.visibility = View.VISIBLE
//    }
//
//    override fun hideForceUpdateLayout() {
//        activity_main_force_update_fl.visibility = View.GONE
//    }

    override fun setForceUpdateMessage(descMaintain: String) {
        activity_main_force_update_tv.text = descMaintain
    }

    override fun getForceUpdateMessage(): String {
        return activity_main_force_update_tv.text.toString()
    }

    override fun toastMessage(msg: String) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
    }

    override fun subscribeApplicationInstall() {
        try {
            bManager = LocalBroadcastManager.getInstance(this)
            val intentFilter = IntentFilter()
            intentFilter.addAction(INSTALLING_PROGRESS)
            bManager?.registerReceiver(applicationInstallBroadcastReceiver, intentFilter)
        } catch (e: NullPointerException) {
            Log.d(TAG, "subscribeApplicationInstall error")
        }

    }
    override fun unSubscribeApplicationInstall() {
        try {
            bManager!!.unregisterReceiver(applicationInstallBroadcastReceiver)
        } catch (e: NullPointerException) {
            Log.d(TAG, "unSubscribeApplicationInstall error")
        }
    }

    override fun setButtonMessage(res: Int) {
        activity_main_install_btn.visibility = View.VISIBLE
        activity_main_install_btn.setText(res)
    }

    override fun showInStallButton() {
        activity_main_install_btn.visibility = View.VISIBLE
    }

    override fun hideInStallButton() {
//        activity_welcome_install_btn.visibility = View.GONE
    }

    override fun subscribeDownloading() {
        try {
            bManager = LocalBroadcastManager.getInstance(this)
            val intentFilter = IntentFilter()
            intentFilter.addAction(DOWNLOAD_STATUS)
            bManager?.registerReceiver(downloadBroadcastReceiver, intentFilter)
        } catch (e: NullPointerException) {
            Log.d(TAG, "subscribeDownloading error")
        }

    }

    override fun unSubscribeDownloading() {
        try {
            bManager?.unregisterReceiver(downloadBroadcastReceiver)
        } catch (e: NullPointerException) {
            Log.d(TAG, "unSubscribeDownloading error")
        }

    }

    override fun startDownloadService(url: String, fileName: String) {
        val intent = Intent(this, DownloadService::class.java)
        intent.putExtra(URL, url)
        intent.putExtra(NAME, fileName.replace(" ", ""))
        this.startService(intent)
    }

    override fun updateProgress(progress: Int) {
        Log.d("downloading", "progress$progress")
        activity_main_progress_circular.progress = progress
        activity_main_progress_tv.text = "$progress %"
    }

    override fun startInstallService(filePath: String) {
        val intent = Intent(Contextor.getInstance().context, InstallService::class.java)
        intent.putExtra(PATH, filePath)
        this.startService(intent)
    }

    override fun clearAppDownloadData() {
        val dir = File(Environment.getExternalStorageDirectory().absolutePath + "/apps/data")
        if (dir.isDirectory) {
            val children = dir.list()
            if (children == null || children.isEmpty()) {
                return
            }

            for (i in children.indices) {
                File(dir, children[i]).delete()
            }
        }
    }
}
